import * as React from "react"
import * as ReactDOM from "react-dom"
import * as immutable from "immutable"

import * as game from "onitama/game"
import * as view from "onitama/client/ui/view"

const cards = immutable.Set([
  {
    name: "Cobra",
    character: "蛇",
    deltas: immutable.Set([
      new game.Coord(0, -1),
      new game.Coord(-1, 1),
      new game.Coord(1, 1),
    ]),
  },
  {
    name: "Dragon",
    character: "竜",
    deltas: immutable.Set([
      new game.Coord(-1, -2),
      new game.Coord(1, -1),
      new game.Coord(1, 1),
      new game.Coord(-1, 2),
    ]),
  },
  {
    name: "Horse",
    character: "馬",
    deltas: immutable.Set([
      new game.Coord(0, -1),
      new game.Coord(-1, 0),
      new game.Coord(1, 0),
    ]),
  },
  {
    name: "Tiger",
    character: "虎",
    deltas: immutable.Set([
      new game.Coord(1, 0),
      new game.Coord(-2, 0),
    ]),
  },
  {
    name: "Frog",
    character: "蛙",
    deltas: immutable.Set([
      new game.Coord(-1, -1),
      new game.Coord(0, -2),
      new game.Coord(1, 1),
    ]),
  },
  {
    name: "Rabbit",
    character: "兎",
    deltas: immutable.Set([
      new game.Coord(1, -1),
      new game.Coord(-1, 1),
      new game.Coord(0, 2),
    ]),
  },
  {
    name: "Crab",
    character: "蟹",
    deltas: immutable.Set([
      new game.Coord(0, -2),
      new game.Coord(-1, 0),
      new game.Coord(0, 2),
    ]),
  },
  {
    name: "Elephant",
    character: "象",
    deltas: immutable.Set([
      new game.Coord(0, -1),
      new game.Coord(-1, -1),
      new game.Coord(-1, 1),
      new game.Coord(0, 1),
    ]),
  },
  {
    name: "Goose",
    character: "鵞鳥",
    deltas: immutable.Set([
      new game.Coord(0, -1),
      new game.Coord(-1, -1),
      new game.Coord(0, 1),
      new game.Coord(1, 1),
    ]),
  },
  {
    name: "Rooster",
    character: "鶏",
    deltas: immutable.Set([
      new game.Coord(0, -1),
      new game.Coord(1, -1),
      new game.Coord(0, 1),
      new game.Coord(-1, 1),
    ]),
  },
  {
    name: "Monkey",
    character: "猿",
    deltas: immutable.Set([
      new game.Coord(-1, -1),
      new game.Coord(-1, 1),
      new game.Coord(1, -1),
      new game.Coord(1, 1),
    ]),
  },
  {
    name: "Mantis",
    character: "蟷螂",
    deltas: immutable.Set([
      new game.Coord(1, 0),
      new game.Coord(-1, -1),
      new game.Coord(-1, 1),
    ]),
  },
  {
    name: "Ox",
    character: "牛",
    deltas: immutable.Set([
      new game.Coord(-1, 0),
      new game.Coord(0, 1),
      new game.Coord(1, 0),
    ]),
  },
  {
    name: "Crane",
    character: "鶴",
    deltas: immutable.Set([
      new game.Coord(-1, 0),
      new game.Coord(1, -1),
      new game.Coord(1, 1),
    ]),
  },
  {
    name: "Boar",
    character: "猪",
    deltas: immutable.Set([
      new game.Coord(0, -1),
      new game.Coord(-1, 0),
      new game.Coord(0, 1),
    ]),
  },
  {
    name: "Eel",
    character: "鰻",
    deltas: immutable.Set([
      new game.Coord(-1, -1),
      new game.Coord(1, -1),
      new game.Coord(0, 1),
    ]),
  },
])

const rules = { extraCards: 0 }

ReactDOM.render(<view.App cards={cards} rules={rules} />, document.getElementById("root"))