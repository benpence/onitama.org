import * as React from "react"
import * as immutable from "immutable"
import * as game from "onitama/game"
import * as util from "onitama/util"
import * as types from "onitama/client/types"

export const ChooseCards = (props: { chosen: immutable.Set<game.Card>, cards: immutable.Set<game.Card>, totalNeeded: number, handleCard: types.CardHandler }) => {
  const cardsNeeded = props.totalNeeded - props.chosen.size
  return (
    <div className="choose-cards">
      <div id="status">Choose {cardsNeeded} more card{cardsNeeded !== 1 ? "s" : ""}</div>
      <div className="cards">
        {[...props.cards].map(card =>
          <Card card={card} isSelected={props.chosen.has(card)} onClick={() => props.handleCard(card)} />
        )}
      </div>
    </div>
  )
}

type Color = "Red" | "Green" | "Blue"

const colorOf = (cardName: string): Color => {
  switch (Math.abs(util.hashString(cardName) % 3)) {
    case 0: return "Red"
    case 1: return "Green"
    case 2: return "Blue"
  }
}

export const Card = (props: { card: game.Card, isSelected?: boolean, onClick: types.OnClick }) => {
  return (
    <div className={`card ${props.isSelected === true ? "selected" : ""}`} onClick={props.onClick}>
      <div className="card-info">
        <div className="card-character">{props.card.character}</div>
        <div className="card-name">{props.card.name}</div>
      </div>
      <div className="card-deltas"><DeltasGrid deltas={props.card.deltas} color={colorOf(props.card.name)} /></div>
    </div>
  )
}

const gridMiddle = new game.Coord(2, 2)

const DeltasGrid = (props: { deltas: immutable.Set<game.Coord>, color: Color }) => {
  const deltaCoords = props.deltas.map(coord =>
    coord.plus(gridMiddle)
  )

  return (
    <table className="deltas-grid">
      <tbody>
        {util.range(0, 5).map(row =>
          <tr className="deltas-row">
            {util.range(0, 5).map(col => {
              const coord = new game.Coord(row, col)

              if (deltaCoords.has(coord)) {
                return <td className={`delta dest ${props.color.toLowerCase()}`} />
              } else if (coord.equals(gridMiddle)) {
                return <td className="delta piece" />
              } else {
                return <td className="delta normal" />
              }
            })}
          </tr>
        )}
      </tbody>
    </table>
  )
}