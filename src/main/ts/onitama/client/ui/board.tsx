import * as React from "react"
import * as immutable from "immutable"
import * as game from "onitama/game"
import * as types from "onitama/client/types"
import * as card from "onitama/client/ui/card"
import * as util from "onitama/util"

type InventoryProps = {
  selectedCard?: game.Card,
  cards: Array<game.Card>,
  handle: types.CardHandler,
}

export const Inventory = (props: InventoryProps) => {
  return (
    <div className="board-cards">
      {props.cards.map(c =>
        <card.Card
          card={c}
          isSelected={props.selectedCard === c}
          onClick={() => props.handle(c)}
        />
      )}
    </div>
  )
}

type BoardProps = {
  board: game.Board,
  currentPlayer: game.Player,
  selectedPiece?: game.Coord,
  selectedCard?: game.Card,
  handle: types.EventHandler,
}

// TODO: Only instantiate when PlayerMove => pass phase params as props
export const Board = (props: BoardProps) => {
  const { board, currentPlayer, selectedPiece, selectedCard, handle } = props
  let dests = immutable.Set<game.Coord>()

  const notPieceForCurrentPlayer = (coord: game.Coord): boolean => {
    const square = board.get(coord)
    return square === "Empty" || currentPlayer !== square.player
  }

  const deltas = selectedCard !== undefined
    ? selectedCard.deltas
    : immutable.Set<game.Coord>()

  if (selectedPiece !== undefined) {
    [...deltas]
      .map(c => currentPlayer === "White"
        ? c.rotate180().plus(selectedPiece)
        : c.plus(selectedPiece)
      )
      .filter(board.inBounds)
      .filter(notPieceForCurrentPlayer)
      .forEach(d => dests = dests.add(d))
  }

  return (
    <table className="grid">
      <tbody>
        {util.range(0, board.height()).map(row =>
          <tr className="grid-row">
            {util.range(0, board.width()).map(col => {
              const coord = new game.Coord(row, col)
              const isArch = coord.equals(new game.Coord(0, 2)) || coord.equals(new game.Coord(4, 2))
              const square = board.get(coord)

              if (coord.equals(selectedPiece)) {
                // Unselect piece
                return <Square piece={square} display="Selected" isArch={isArch} onClick={() => handle({ type: "SelectPiece" })} />

              } else if (dests.has(coord)) {
                // Select destination
                return <Square piece={square} display="Dest" isArch={isArch} onClick={() => handle({ type: "SelectDest", dest: coord })} />

              } else if (square !== "Empty" && currentPlayer === square.player) {
                // Select piece
                return <Square piece={square} display="Normal" isArch={isArch} onClick={() => handle({ type: "SelectPiece", piece: coord })} />

              } else {
                // Display normally, non interactive
                return <Square piece={square} display="Normal" isArch={isArch} onClick={() => undefined} />
              }
            })}
          </tr>
        )}
      </tbody>
    </table>
  )
}

type SquareDisplay = "Normal" | "Selected" | "Dest"

const svgFile = (piece: game.Piece): string => {
  if (piece.player === "White") {
    if (piece.type === "Master") {
      return "solid_king.svg"
    } else {
      return "solid_pawn.svg"
    }
  } else {
    if (piece.type === "Master") {
      return "empty_king.svg"
    } else {
      return "empty_pawn.svg"
    }
  }
}

const Square = (props: { piece: game.Square, display: SquareDisplay, isArch: boolean, onClick: types.OnClick }) => {
  const pieceElement = props.piece !== "Empty"
    ? <img src={`static/img/${svgFile(props.piece)}`} />
    : <></>

  const squareClasses = props.piece !== "Empty"
    ? `${props.piece.player.toLowerCase()} ${props.piece.type.toLowerCase()}`
    : ""

  const className = `grid-square ${squareClasses} ${props.isArch ? "arch" : ""} ${props.display.toLowerCase()}`

  return <td className={className} onClick={props.onClick}>{pieceElement}</td>
}