import * as React from "react"
import * as immutable from "immutable"
import * as game from "onitama/game"
import * as board from "onitama/client/ui/board"
import * as card from "onitama/client/ui/card"
import * as types from "onitama/client/types"

export const App = (props: { cards: immutable.Set<game.Card>, rules: game.Rules }) => {
  const [ongoingGame, setGame] = React.useState(game.Game.from(props.cards, props.rules))
  const [history, setHistory] = React.useState([])


  const handle = (event: types.UIEvent): void => {

    if (event === "NewCards") {
      const newGame = game.Game.from(props.cards, props.rules)
      setHistory([])
      setGame(newGame)
      console.log(event, newGame)

    } else if (event === "NewGame") {
      const newGame = ongoingGame.newGame()
      setHistory([])
      setGame(newGame)
      console.log(event, newGame)

    } else if (event === "Undo" && history.length > 0) {
      const previousGame = history[history.length - 1]
      setHistory(history.slice(0, history.length - 1))
      setGame(previousGame)
      console.log(event, previousGame)

    } else if (types.uiEventIsGameAction(event) && ongoingGame.status() === "Ongoing") {
      const newGame = ongoingGame.handleEvent(event)

      if (ongoingGame.phase.type === "PlayerMove") {
        setHistory([...history, ongoingGame])
      }

      setGame(newGame)
      console.log(event, newGame)
    }
  }

  const originalGame = <a href="https://www.arcanewonders.com/game/onitama/">original game</a>
  const source = <a href="https://gitlab.com/benpence/onitama.org">source</a>

  return (
    <div id="app">
      <h1 id="title">Onitama ({originalGame}) ({source})</h1>
      <Game ongoingGame={ongoingGame} handle={handle} />
    </div>
  )
}

const Game = (props: { ongoingGame: game.Game, handle: types.EventHandler }) => {
  const phase = props.ongoingGame.phase
  if (phase.type === "ChooseCards") {
    const handleCard = (card: game.Card): void => {
      props.handle({ type: "ChooseCard", card: card })
    }

    return (
      <card.ChooseCards
        chosen={phase.chosen}
        cards={phase.cards}
        totalNeeded={game.Game.normalCards + props.ongoingGame.rules.extraCards}
        handleCard={handleCard}
      />
    )
  } else if (phase.type === "PlayerMove") {
    const handleCard = (owner?: game.Player): types.CardHandler => {
      return (c: game.Card): void => {
        if (owner === phase.player) {
          if (phase.selectedPiece !== undefined && c === phase.selectedCard) {
            // Unselect
            props.handle({ type: "SelectCard" })
          } else if (phase.selectedPiece !== undefined) {
            // Select card
            props.handle({ type: "SelectCard", card: c })
          }
        }
      }
    }

    const inventory = (cards: Array<game.Card>, className: string, handle: types.CardHandler, rotateCards?: boolean) => {
      return (
        <div className={className}>
          <board.Inventory selectedCard={phase.selectedCard} cards={cards} handle={handle} />
        </div>
      )
    }

    let selectObject
    if (!phase.selectedPiece) {
      selectObject = "one of your pieces"
    } else if (!phase.selectedCard) {
      selectObject = "one of your cards"
    } else {
      selectObject = "a destination for your piece"
    }

    const status = props.ongoingGame.status()
    const statusMessage = status === "Ongoing"
      ? `${phase.player}'s turn. Select ${selectObject}.`
      : `${status.player} Wins!`

    return (
      <div id="game">
        <div id="controls">
          <div id="status">{statusMessage}</div>
          <div className="buttons">
            <button id="new-cards" onClick={() => props.handle("NewCards")}>New Cards</button>
            <button id="new-game" onClick={() => props.handle("NewGame")}>Restart Game</button>
            <button id="undo" onClick={() => props.handle("Undo")}>Undo</button>
          </div>
        </div>
        <div id="board" className={phase.player === "White" ? "white" : "black"} >
          {inventory([...phase.inventory.white], "white-cards", handleCard("White"), true)}
          <div className="middle">
            {inventory([...phase.inventory.toWhite], "to-white-cards", handleCard())}
            <board.Board
              board={props.ongoingGame.board}
              currentPlayer={phase.player}
              selectedPiece={phase.selectedPiece}
              selectedCard={phase.selectedCard}
              handle={props.handle}
            />
            {inventory([...phase.inventory.toBlack], "to-black-cards", handleCard())}
          </div>
          {inventory([...phase.inventory.black], "black-cards", handleCard("Black"))}
        </div>
      </div>
    )
  }
}
