import * as game from "onitama/game"

export type UIEvent = "NewCards" | "NewGame" | "Undo" | game.Event
export type EventHandler = (event: UIEvent) => void
export type CardHandler = (card?: game.Card) => void
export type OnClick = () => void
export const doNothing = () => undefined
export const uiEventIsGameAction = (event: UIEvent): event is game.Event => {
  return event !== "NewCards" && event !== "NewGame" && event !== "Undo"
}