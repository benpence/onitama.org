import * as util from "onitama/util"
import * as immutable from "immutable"

export class Coord {
  public readonly row: number
  public readonly col: number

  constructor(row: number, col: number) {
    this.row = row
    this.col = col
  }

  public plus = (other: Coord): Coord => {
    return new Coord(this.row + other.row, this.col + other.col)
  }

  public rotate180 = (): Coord => {
    return new Coord(this.row * -1, this.col * -1)
  }

  public equals = (other: any): boolean => {
    if (other === null || other === undefined) {
      return false
    }
    return (other as Coord).row === this.row && (other as Coord).col === this.col
  }

  public hashCode = (): number => {
    return 1000 * this.row + this.col
  }

  public toString = () => `(${this.row}, ${this.col})`
}

export type PieceType = "Master" | "Student"

export interface Piece {
  readonly player: Player,
  readonly type: PieceType,
}

export type Square = Piece | "Empty"

export class Board {
  readonly squares: Array<Array<Square>>

  static start = new Board([
    [
      { player: "White", type: "Student" },
      { player: "White", type: "Student" },
      { player: "White", type: "Master" },
      { player: "White", type: "Student" },
      { player: "White", type: "Student" },
    ],
    ["Empty", "Empty", "Empty", "Empty", "Empty"],
    ["Empty", "Empty", "Empty", "Empty", "Empty"],
    ["Empty", "Empty", "Empty", "Empty", "Empty"],
    [
      { player: "Black", type: "Student" },
      { player: "Black", type: "Student" },
      { player: "Black", type: "Master" },
      { player: "Black", type: "Student" },
      { player: "Black", type: "Student" },
    ],
  ])

  constructor(squares: Array<Array<Square>>) {
    if (squares.length === 0) {
      throw "Received empty array of rows while constructing Board"
    }

    this.squares = squares
  }

  public height = (): number => this.squares.length
  public width = (): number => this.squares[0].length

  public get = (coord: Coord): Square => {
    if (!this.inBounds(coord)) {
      throw `Cannot get coordinates outside board (${this.height()} height, ${this.width()} width): ${coord.toString()}: `
    }

    return this.squares[coord.row][coord.col]
  }

  public move = (from: Coord, to: Coord): Board => {
    const newSquares = new Array(this.squares.length)

    for (let row = 0; row < newSquares.length; row++) {
      newSquares[row] = new Array(this.squares[row].length)
    }

    for (let [coord, square] of this.cells()) {
      newSquares[coord.row][coord.col] = square
    }

    newSquares[from.row][from.col] = "Empty"
    newSquares[to.row][to.col] = this.get(from)

    return new Board(newSquares)
  }

  public inBounds = (coord: Coord): boolean => {
    return 0 <= coord.row &&
      0 <= coord.col &&
      coord.row < this.height() &&
      coord.col < this.width()
  }

  public cells = function* (): IterableIterator<[Coord, Square]> {
    for (let row = 0; row < this.height(); row++) {
      for (let col = 0; col < this.width(); col++) {
        yield [new Coord(row, col), this.squares[row][col]]
      }
    }
  }
}

export type Player = "White" | "Black"

export interface Card {
  readonly name: string,
  readonly character: string,
  readonly deltas: immutable.Set<Coord>,
}

export interface Inventory {
  readonly white: immutable.Set<Card>,
  readonly toBlack: Array<Card>,
  readonly black: immutable.Set<Card>,
  readonly toWhite: Array<Card>,
}

export interface ChooseCards {
  readonly type: "ChooseCards",
  readonly chosen: immutable.Set<Card>,
  readonly cards: immutable.Set<Card>,
}
export interface PlayerMove {
  readonly type: "PlayerMove",
  readonly player: Player,
  readonly selectedPiece?: Coord,
  readonly selectedCard?: Card,
  readonly inventory: Inventory,
}
export type Phase = ChooseCards | PlayerMove


export interface ChooseCard {
  readonly type: "ChooseCard",
  readonly card: Card,
}
export interface SelectPiece {
  readonly type: "SelectPiece",
  readonly piece?: Coord,
}
export interface SelectCard {
  readonly type: "SelectCard",
  readonly card?: Card,
}
export interface SelectDest {
  readonly type: "SelectDest",
  readonly dest: Coord,
}

export type Event = ChooseCard | SelectPiece | SelectCard | SelectDest

export interface Rules {
  readonly extraCards: number,
}

export interface Winner {
  readonly player: Player,
}
export type Status = Winner | "Ongoing"

export class Game {
  readonly board: Board
  readonly phase: Phase
  readonly rules: Rules

  static from = (cards: immutable.Set<Card>, rules: Rules): Game => {
    const phase: ChooseCards = {
      type: "ChooseCards",
      cards: cards,
      chosen: immutable.Set()
    }

    return new Game(
      Board.start,
      phase,
      rules
    )
  }

  constructor(board: Board, phase: Phase, rules: Rules) {
    this.board = board
    this.phase = phase
    this.rules = rules
  }

  public newGame = (): Game => {
    const inventory = (this.phase as PlayerMove).inventory

    let cards = immutable.Set()
    for (let set of [inventory.white, inventory.black, inventory.toBlack, inventory.toWhite]) {
      for (let card of set) {
        cards = cards.add(card)
      }
    }

    const newPhase: PlayerMove = {
      type: "PlayerMove",
      player: "White",
      inventory: this.shuffleInventory(cards),
    }

    return new Game(Board.start, newPhase, this.rules)
  }

  public status = (): Status => {
    let pieces = immutable.Set<[Coord, Piece]>()

    for (let [coord, piece] of this.board.cells()) {
      if (piece !== "Empty") {
        pieces = pieces.add([coord, piece])
      }
    }

    // Check if master has been captured
    if (!pieces.find(p => p[1].player === "White" && p[1].type === "Master")) {
      return { player: "Black" }

    } else if (!pieces.find(p => p[1].player === "Black" && p[1].type === "Master")) {
      return { player: "White" }
    }

    // Check if gate is captured
    if (pieces.find(p => p[1].player === "White" && p[1].type === "Master" && p[0].row === 4 && p[0].col === 2)) {
      return { player: "White" }

    } else if (pieces.find(p => p[1].player === "Black" && p[1].type === "Master" && p[0].row === 0 && p[0].col === 2)) {
      return { player: "Black" }
    }

    return "Ongoing"
  }

  public handleEvent = (event: Event): Game => {
    switch (event.type) {
      case "ChooseCard":
        return this.chooseCard(
          (this.phase as ChooseCards).chosen,
          (this.phase as ChooseCards).cards,
          event.card,
        )
      case "SelectPiece":
        return this.selectPiece(
          (this.phase as PlayerMove).player,
          (this.phase as PlayerMove).inventory,
          event.piece,
        )
      case "SelectCard":
        return this.selectCard(
          (this.phase as PlayerMove).player,
          (this.phase as PlayerMove).inventory,
          (this.phase as PlayerMove).selectedPiece,
          event.card,
        )
      case "SelectDest":
        return this.selectDest(
          (this.phase as PlayerMove).player,
          (this.phase as PlayerMove).inventory,
          (this.phase as PlayerMove).selectedPiece,
          (this.phase as PlayerMove).selectedCard,
          event.dest,
        )
    }
  }

  static normalCards = 5

  chooseCard = (chosen: immutable.Set<Card>, cards: immutable.Set<Card>, card: Card): Game => {
    const totalCards = Game.normalCards + this.rules.extraCards

    const newChosen = chosen.has(card)
      ? chosen.delete(card)
      : chosen.add(card)

    if (newChosen.size === totalCards) {
      // We've chosen enough cards -> Start the game
      const newPhase: PlayerMove = {
        type: "PlayerMove",
        player: "White",
        inventory: this.shuffleInventory(newChosen),
      }

      return new Game(this.board, newPhase, this.rules)

    } else {
      // Not enough cards yet -> Choose more cards

      const newPhase: ChooseCards = {
        type: "ChooseCards",
        chosen: newChosen,
        cards,
      }

      return new Game(this.board, newPhase, this.rules)
    }
  }

  shuffleInventory = (cards: immutable.Set<Card>): Inventory => {
    const shuffledCards = util.shuffle([...cards])
    const toBlackNum = (Game.normalCards - 4 + this.rules.extraCards) / 2

    return {
      white: immutable.Set(shuffledCards.slice(0, 2)),
      toBlack: shuffledCards.slice(4, 4 + toBlackNum),
      black: immutable.Set(shuffledCards.slice(2, 4)),
      toWhite: shuffledCards.slice(4 + toBlackNum),
    }
  }

  selectPiece = (player: Player, inventory: Inventory, piece: Coord): Game => {
    const newPhase: PlayerMove = {
      type: "PlayerMove",
      player,
      selectedPiece: piece,
      inventory,
    }

    return new Game(this.board, newPhase, this.rules)
  }

  selectCard = (player: Player, inventory: Inventory, piece: Coord, card: Card): Game => {
    const newPhase: PlayerMove = {
      type: "PlayerMove",
      player,
      selectedPiece: piece,
      selectedCard: card,
      inventory,
    }

    return new Game(this.board, newPhase, this.rules)
  }

  selectDest = (player: Player, inventory: Inventory, piece: Coord, card: Card, dest: Coord): Game => {
    const newPlayer = player === "White" ? "Black" : "White"

    // TODO: Create queue data structure in util
    const newInventory = player === "White"
      ? {
        ...inventory,
        toWhite: inventory.toWhite.slice(1),
        white: inventory.white.delete(card).add(inventory.toWhite[0]),
        toBlack: [...inventory.toBlack, card],
      }
      : {
        ...inventory,
        toBlack: inventory.toBlack.slice(1),
        blue: inventory.black.delete(card).add(inventory.toBlack[0]),
        toWhite: [...inventory.toWhite, card],
      }

    const newBoard = this.board.move(piece, dest)

    const newPhase: PlayerMove = {
      type: "PlayerMove",
      player: newPlayer,
      inventory: newInventory,
    }

    return new Game(newBoard, newPhase, this.rules)
  }
}