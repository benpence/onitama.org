export const range = (start: number, end: number): Array<number> => {
  const arr = Array(end - start)

  for (let i = start; i < end; i++) {
    arr[i - start] = i
  }

  return arr
}

export function setAdd<A>(set: Set<A>, elem: A): Set<A> {
  return new Set([...set, elem])
}

export function setDelete<A>(set: Set<A>, elem: A): Set<A> {
  const newSet = new Set([...set])
  newSet.delete(elem)
  return newSet
}

export function shuffle<A>(array: Array<A>): Array<A> {
  const output = [...array]

  for (let i = output.length - 1; 0 < i; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [output[i], output[j]] = [output[j], output[i]];
  }

  return output
}

export const hashString = (str: string): number => {
  let hash = 0
  if (str.length === 0) return hash;
  for (let i = 0; i < str.length; i++) {
    const chr = str.charCodeAt(i)
    hash = ((hash << 5) - hash) + chr
    hash |= 0
  }

  return hash
}