I loved playing [Onitama](https://www.arcanewonders.com/game/onitama/). This project is my attempt to recreate the game's fundamental features for the web. If you find this interesting, go buy the physical game!

# Install

```
$ npm install
```

# Run

```
$ npm run client-build
$ npm run server-prod
```

Visit `http://localhost:3000` to try out.
